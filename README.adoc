:icons: font
:toc: left
:source-highlighter: pygments

== Groundhog Dojo

Groundhog Dojo is a platform for polishing coding skills using simple tasks and a small dose of stress. The inspiration for it came from:

* https://en.wikipedia.org/wiki/Kata_(programming)[code kata], an exercise for sharpening coding skills through repetition. The concept itself was borrowed from martial arts, in which constantly repeating the same sequence of moves (kata) lets the adepts train the techniques to the level at which they don't need to think about them.
* https://en.wikipedia.org/wiki/Groundhog_Day_(film)[Groundhog Day], a movie where the main character is forced to relive the same day over and over again. At some point, he starts using that to learn new things (play the piano or speak French).
* https://www.imdb.com/title/tt0709042/["Stargate SG-1" Avatar (S08E06)] featuring a VR game developed for the purpose of training soldiers. The game replays the scenario over and over, adjusting the difficulty level to the player.

The main idea is as follows:

. During each round, the app gives you a task.
. You have a fixed amount of time to solve it.
. When the time is up, the script checks your solution.
. If the test fails, you start from scratch (with slightly increased amount of time).
. Otherwise, you can choose to:
* try the same task again (with slightly decreased amount of time),
* take another task (with regular time limit).

=== Prerequisites

Scripts in this game assume you have a UNIX-like OS with the following tools on PATH:

* Bash 4+,
* Git,
* Make,
* Vim/Emacs/Nano,
* Tmux,
* JDK 11+/Python 3+/Erlang/whatever supported environment you want to practice.

WARNING: While it's theoretically possible to satisfy these requirements on Windows using either WSL or Git Bash, GD was never tested for running in such environment. Feel free to check it out and file an issue should any changes be necessary.

=== Installation

The easiest way is to clone the project's repository. GD will simply utilize the existing repo.

Another method is to download the sources and unpack the archive. On first startup, GD will initialize a Git repository for its purposes.

Groundhod Dojo can run in several modes:

Single-user mode (gd.sh):: The app is started on the local machine with the runtime data being stored in the project directory.
Multi-user mode (gd-multi.sh):: The app is started on the local machine, but the runtime data is stored in a temporary folder (per process).
Web mode (gd-web.sh):: The app starts a web server which makes GD available via a web browser.

All modes above require the prerequisites to be satisfied. In addition, there are two Docker configuration files provided:

* one which runs GD in a single-user mode with all deps installed,
* one which runs GD in a web mode, tailored for deployment on Heroku.

=== How to play?

To run the app, simply execute one of the launcher scripts (`gd*.sh`) in the root directory of the project.

Running the script without arguments starts the default mode with a simple menu.

GD can also be started with a task/task group passed as an argument. In such case, the program jumps directly to the coding screen. The script starts an editor (Vim by default) on the left, a shell on the bottom, a task description on the right, and a timer (right top).

During each round, try to implement the solution. You can create your own methods, classes, or even packages. The only condition is that you call your code from the `main` method of the `Main` class (or different setup depending on the chosen environment), located in the default package.

=== How to practice?

. If the default time for a task is to short at the beginning, you can start with your own time limit and reduce it as you feel more confident.
. Many factors can affect your speed of coding, like:
* Knowledge of the editor (commands, shortcuts, tricks),
* Knowledge of the language's syntax,
* Knowledge of the API,
* Knowledge of the algorithms.
+
You can use this game to focus on a particular aspect you want to master.
. For each round, a Git branch is created and/or checked out. That means, you can:
* preserve the last solution for the given task,
* save the last solution (on another branch) and try again,
* save the current state (on the current branch) as a base for next iterations.
. Many scenarios of the game are possible:
* Keep coding the same approach until you can't make mistake (fixed time).
* Optimize coding speed by gradually decreasing the time limit.
* Change the approach on each iteration:
** Various programming styles (all-in-main, procedural, OOP, functional)
** Various algorithms (iterative, recursive, divide & conquer)
** Various restrictions (no conditionals, no loops, no variables)
* Practice TDD, committing the changes on each "green" stage of cycle.
. In every moment of coding, you can:
* Check if the code compiles
+
....
$ make
....
+
* Run the current Java file
+
....
$ run [args]
....
+
* Check if the solution passes the tests:
+
....
$ make test
....
+
. Have fun!
