FROM ubuntu:latest
RUN useradd -U -l -d /gd gd
COPY --chown=gd:gd . /gd
RUN DEBIAN_FRONTEND="noninteractive" TZ="Europe/London" apt-get update && apt-get -y install tzdata
RUN apt-get update && apt-get -y install \
 git \
 make \
 tmux \
 nano \
 vim-nox \
 emacs-nox
RUN apt-get update && apt-get -y install openjdk-16-jdk-headless \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
USER gd
WORKDIR /gd
ENTRYPOINT ["./gd.sh"]
