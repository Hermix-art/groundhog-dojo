#!/bin/bash

export GD_ROOT="${GD_ROOT:-$(cd $(dirname "$0") && pwd)}"
export GD_WORK="${GD_WORK:-"/tmp/gd-$$"}"
export GD_MULTI=1

export PATH=${GD_ROOT}/bin:${PATH}

[[ -d ${GD_WORK} ]] && rm -r "${GD_WORK}"

exec ${GD_ROOT}/gd.sh "$@"
