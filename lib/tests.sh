#!/usr/bin/env bash

source ${GD_ROOT}/lib/util.sh

unset SCENARIO INPUT_ARGS INPUT_STREAM OUTPUT EXPECTED ASSERTION

STATUS=0
PASSED=0
FAILED=0

scenario:() {
    SCENARIO="${*}"
    if [ -n "${SCENARIO}" ]; then
        echo "Scenario: ${SCENARIO}"
    fi
}

command:() {
    GD_TEST_COMMAND="${*}"
}

input:() {
    INPUT_ARGS=("${@}")
    [[ -t 0 ]] || INPUT_STREAM=$(cat)
    echo "Input args: <${INPUT_ARGS[@]}>"
    echo "Input stream: <${INPUT_STREAM}>"

}

output:() {
    if [[ $# -gt 0 ]]; then
        EXPECTED="${*}"
    elif [[ ! -t 0 ]]; then
        EXPECTED="$(cat)"
    else
        EXPECTED=""
    fi
    echo "Expected: <${EXPECTED[@]}>"
    local OUTPUT="$(timeout 10s ${GD_TEST_COMMAND} "${INPUT_ARGS[@]}" <<< "${INPUT_STREAM}")"
    echo "Actual: <${OUTPUT}>"
    test "${EXPECTED}" = "${OUTPUT}" && pass || fail
    local CODE=$?
    ((PASSED+=(${CODE}==0)))
    ((FAILED+=(${CODE}!=0)))
    STATUS=${FAILED}
    return ${STATUS}
}

assert:() {
    ASSERTION="${1}"
    EXPECTED="${2}"
    echo "Assertion: <${ASSERTION}>"
    local OUTPUT="$(timeout 10s ${GD_TEST_COMMAND} "${INPUT_ARGS[@]}" <<< "${INPUT_STREAM}")"
    echo "Output: <${OUTPUT}>"
    local RESULT=$(eval ${ASSERTION})
    test "${EXPECTED}" = ${RESULT} && pass || fail
    local CODE=$?
    ((PASSED+=(${CODE}==0)))
    ((FAILED+=(${CODE}!=0)))
    STATUS=${FAILED}
    return ${STATUS}
}

summary() {
    echo -e "Tests passed: ${PASSED}/$((PASSED+FAILED)) ($((PASSED*100/(PASSED+FAILED)))%)"
    return ${STATUS}
}

---() {
    # Does nothing except separating test scenarios
    return ${STATUS}
}

