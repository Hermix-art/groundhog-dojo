#!/usr/bin/env bash

ask() {
    local question=${1} answer
    read -t 1 -n 10000 _
    read -p "${question} [Y/n] "
    echo
    [[ -z "${REPLY// }" || "${REPLY:0:1}" == "Y" || "${REPLY:0:1}" == "y" ]]
}

result() {
    local passed=${1:?}
    [[ ${passed} = "true" ]] && pass || fail
}

pass() {
    echo -e "\e[32mPASSED\e[0m"; return 0
}

fail() {
    echo -e "\e[31mFAILED\e[0m"; return 1
}

die() {
    echo "$1" >&2 ; exit 1
}

ftime() {
    if [[ ${1} = "-s" ]]; then
        local sep="${2}"; shift 2
    else
        local sep=":"
    fi 
    local time=${1:?}
    local _var=${2:-time}
    local min=$(( ${time} / 60 ))
    local sec=$(( ${time} % 60 ))
    printf -v ${_var} "%02d%s%02d" ${min} "${sep}" ${sec}
    if [[ -z ${2} ]]; then
        echo ${time}
    fi
}

cleanup() {
    local branch="${1:?}"
    
    make clean \
        && git reset --hard HEAD \
        && git clean -f \
        && find ${GD_WORK}/src -name '.*.sw*' | xargs -r rm \
        && git switch -f -C "${branch}" master \
        || die
} &> /dev/null

export -f ftime
export -f cleanup

