#!/usr/bin/env bash

menu() {
    while true; do
        welcome
        
        echo ""
        echo $'\e[1m'"Settings:"$'\e[0m'
        echo ""
        echo "  Working dir: ${GD_WORK}"
        echo "  Editor:      ${GD_EDITOR}"
        echo "  Environment: ${GD_ENV}"
        echo ""
        echo $'\e[1m'"Main menu (0=Quit):"$'\e[0m'
        echo ""

        select action in "Start!" "Settings" "About GD" "License"; do
            case ${action} in
                About*)
                    about
                    continue 2
                    ;;
                License*)
                    license
                    continue 2
                    ;;
                Settings*)
                    select_env
                    select_editor
                    continue 2
                    ;;
                Start*)
                    select_task_group "$@"
                    continue 2
                    ;;
                *)
                    echo "Bye!"
                    break 2
            esac
        done
    done
}

welcome() {
    clear
    cat ${GD_ROOT}/welcome
}

about() {
    clear
    cat ${GD_ROOT}/about
    read -p "[Press Enter to continue...]"
}

license() {
    clear
    cat ${GD_ROOT}/LICENSE
    read -p "[Press Enter to continue...]"
}

check_env() {
    make -f ${GD_ENVS}/${1}/Makefile deps 2> /dev/null || {
        echo "" >&2
        echo $'\e[33m'"The selected environment is not installed."$'\e[0m' >&2
        echo "" >&2
        return 1
    }
}

select_env() {
    local env

    echo ""
    echo $'\e[1m'"Environment (0=${GD_ENV}):"$'\e[0m'
    echo ""

    select env in $(cd ${GD_ENVS} && find . -type d -name '[^.]*' | xargs -l basename); do
        [[ -z ${env} ]] && break
        if check_env "${env}"; then
            export GD_ENV="${env}"
            break
        fi
    done
}

check_editor() {
    make -f ${GD_EDITORS}/${1}/Makefile deps 2> /dev/null || {
        echo "" >&2
        echo $'\e[33m'"The selected editor is not installed."$'\e[0m' >&2
        echo "" >&2
        return 1
    }
}

select_editor() {
    local editor

    echo ""
    echo $'\e[1m'"Editor (0=${GD_EDITOR}):"$'\e[0m'
    echo ""

    select editor in $(cd ${GD_EDITORS} && find . -type d -name '[^.]*' | xargs -l basename); do
        [[ -z ${editor} ]] && break
        if check_editor "${editor}"; then
            export GD_EDITOR="${editor}"
            break
        fi
    done
}

select_task_group() {
    while true; do

        echo ""
        echo $'\e[1m'"Select a task group (0=Main menu):"$'\e[0m'
        echo ""

        select task_group in $(list_task_groups | sort); do
            if [[ -n ${task_group} ]]; then
                start_task_group "${task_group}"
                continue 2
            else
                break 2
            fi
        done
    done
}

