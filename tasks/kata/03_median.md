# Median

Write a program which, given a list of numbers as arguments, prints a median of the numbers in that lists.

Median of a set of numbers is a number (not necessarily belonging to the list) for which there is exactly the same number of smaller and greater numbers.

Example:
```
$ run 5 4 6 3 1 2
3.5
```
