# Find number 3 in sorted list

Write a program which, when given a sorted list of numbers as arguments, prints the index (starting with 0) of the number 3 in the list, or -1, if there's no such number.

Example:
```
$ run 2 3 4 5 6 7
1
```
