# Fibonacci sequence

Write program which, when given a number N as an argument, prints N-th number in Fibonacci sequence.

N-th number in Fibonacci sequence (or: N-th Fibonacci number) is:

* 0, when N = 0,
* 1, when N = 1,
* Sum of (N-1)-th and (N-2)-th Fibonacci
  number.

Example:
```
$ run 7
13
```
