# Duplicate number

Write a program which, when given a sequence of numbers as arguments, prints the number which is provided twice in this sequence.

Example:
```
$ run 5 4 6 3 5 2
5
```
