# Rotate left

Write a program which, when given a list of numbers as arguments, prints the list rotated left by 1.

Example:
```
$ run 1 2 3 4 5 6
2 3 4 5 6 1 
```
