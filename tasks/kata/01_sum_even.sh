#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:
output: 0
---
input:  5
output: 0
---
input:  5 4
output: 4
---
input:  5 4 6
output: 10
---
input:  5 4 6 3
output: 10
---
input:  5 4 6 3 7 2
output: 12
---
summary
