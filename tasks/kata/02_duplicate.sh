#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:
output:
---
input: 1
output:
---
input: 1 2
output:
---
input: 1 1
output: 1
---
input:  5 4 6 3 5 2
output: 5
---
summary
