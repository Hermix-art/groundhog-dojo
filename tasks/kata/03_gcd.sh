#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  1 1                  
output: 1
---
input:  1 2                  
output: 1
---
input:  2 3                  
output: 1
---
input:  2 4                  
output: 2
---
input:  12 8                 
output: 4
---
input:  1836311903 1134903170
output: 1
---
input:  1134903170 1836311903
output: 1
---
summary
