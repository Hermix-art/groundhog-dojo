#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  0
output: 0
---
input:  1
output: 1
---
input:  2
output: 1
---
input:  3
output: 2
---
input:  7
output: 13
---
input:  46
output: 1836311903
---
summary
