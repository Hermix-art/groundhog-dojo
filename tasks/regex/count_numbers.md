# Count numbers in given string

Write a program which, when given a string as argument, prints the quantity of numbers it contains.

Example:
```
$ run "\
> Why did the Star Wars episodes 4, 5 \
> and 6 come before 1, 2 and 3? Because \
> in charge of planning, Yoda was."
6
```
