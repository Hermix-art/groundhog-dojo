#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  "quack" ""
output: 0
---
input:  "much" "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
output: 1
---
input:  "a" "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
output: 2
---
input:  "wood" "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
output: 4
---
input:  "wo" "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
output: 5
---
input:  " " "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
output: 12
---
summary
