# Count occurrences

Write a program which, when given two strings as arguments, prints the number of occurrences of the first string in the second one.

Example:
```
$ run "Java" "JavaScript != Java"
2
```
