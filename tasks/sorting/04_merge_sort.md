# Merge Sort

Write a program which, when given an array of integers, returns the numbers sorted ascendingly using Merge Sort algorithm.

Merge Sort works by dividing the array in two, (merge-)sorting each of the parts, and then merging the sorted parts back into one.

Example:
```
$ run 8 1 6 3 4 5 2 7
1 2 3 4 5 6 7 8
```
