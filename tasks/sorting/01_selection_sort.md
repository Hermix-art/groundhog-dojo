# Selection Sort

Write a program which, when given an array of integers, returns the numbers sorted ascendingly using Selection Sort algorithm.

Selection Sort consists of N rounds, in each of which, the biggest element of the not-yet-sorted part of the array is swapped with the last one in that part, hence becoming the smallest one in the already-sorted part.

Example:
```
$ run 8 1 6 3 4 5 2 7
1 2 3 4 5 6 7 8
```
