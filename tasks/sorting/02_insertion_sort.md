# Insertion Sort

Write a program which, when given an array of integers, returns the numbers sorted ascendingly using Insertion Sort algorithm.

Insertion Sort consists of N rounds, during which, the first element in the not-yet-sorted part of the array is moved towards the beginning by being repetitively swapped with previously processed elements, and eventually landing in a proper place of the so-far-sorted part.

Example:
```
$ run 8 1 6 3 4 5 2 7
1 2 3 4 5 6 7 8
```
