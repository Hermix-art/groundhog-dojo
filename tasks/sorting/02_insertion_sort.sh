#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh
source ${GD_ROOT}/lib/test_util.sh

input:
output: 
---
input:  8
output: 8
---
input:  8 1
output: 1 8
---
input:  1 8
output: 1 8
---
input:  8 1 6
output: 1 6 8
---
input:  8 6 1
output: 1 6 8
---
input:  1 6 8
output: 1 6 8
---
input:  $(rseq 113 101 101)
output: $(seq -s ' ' 1 112)
---
summary
