#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input:  
output: 0
---
input:  5
output: 5
---
input:  5 4
output: 9
---
input:  5 4 6
output: 15
---
input:  5 4 6 3
output: 18
---
input:  5 4 6 3 7 2
output: 27
---
summary
