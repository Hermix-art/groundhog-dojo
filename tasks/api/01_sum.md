# Sum of numbers

Write a program which, when given a list of numbers as arguments, prints the sum of all numbers in the list.

Example:
```
$ run 5 4 6 3 7 2
27
```
