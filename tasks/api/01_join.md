# Join strings

Write a program which, when given a list of strings as arguments, prints all the strings joined in one with no separator.

Example:
```
$ run a b c d e f g h
abcdefgh
```
