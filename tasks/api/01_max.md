# Largest number

Write a program which, when given a sequence of numbers an argument, prints the largest number in this sequence.

Example:
```
$ run 5 4 6 3 7 2
7
```
