#!/usr/bin/env bash

source ${GD_ROOT}/lib/tests.sh

input: 
output: 
---
input:  q
output: q
---
input:  qu
output: uq
---
input:  qua
output: auq
---
input:  quac
output: cauq
---
input:  quack
output: kcauq
---
input:  aibohphobia
output: aibohphobia
---
summary
