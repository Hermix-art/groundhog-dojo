#!/bin/bash

export PORT=${1:-${PORT:-"8080"}}; shift

while [[ "${*}" = *.sh* ]]; do shift || break; done

export GD_ROOT=${GD_ROOT:-$(cd $(dirname $0)/. && pwd)}

if [[ -n ${GD_REPO_URL} ]]; then
    rm -r ${GD_ROOT}/tasks
    git clone --depth=1 ${GD_REPO_REV:+--branch ${GD_REPO_REV}} ${GD_REPO_URL} ${GD_ROOT}/tasks
    rm -r ${GD_ROOT}/tasks/.git
fi

exec ${GD_ROOT}/gotty \
    --port "${PORT#\\}" \
    --permit-write \
    --permit-arguments \
    --title-format "Groundhog Dojo" \
    ${GD_ROOT}/gd-multi.sh "$@"
